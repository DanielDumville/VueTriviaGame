# vue-trivia-game

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Assignment Five for Experis Academy and Noroff – Building a Quiz as a Single Page Application with Vue.

This project was structured around the specifications outlined by Noroff which are summarised below:

-	Build an online trivia game as a Single Page Application using the Vue.js framework (version 2.x).
-	Design a wireframe using Figma
-	Write HTML % CSS as needed
-	Use the Vue.js framework to build the following screens into your trivia game:
    a) The Start screen 
    b) The Question screen 
    c) The Result screen

The Start Screen
a) Your app should start on a “Start Screen.”
 b) The user must be able to select the difficulty, number of questions and category. 
c) The user must click a button or anywhere on the screen to start playing.

The Question Screen
a) Once the game starts, the app must fetch the questions from the API setup in the previous step. The app must ONLY display ONE question at a time. 
b) If it is multiple choice, have 4 buttons with each answer as the text. If it is a True/False question, only display 2 buttons, one for True and one for False.
 c) Once a question is answered the app must move on to the next question. 
d) When all the questions have been answered the user must be presented with the result screen.

The Result Screen
a) The result screen must show all the questions along with the correct and user’s answers.
 b) Display the total score. 
c) Display a button to take the user back to the start screen or replay with different questions, but at the current difficulty level and category.

The data to be used in the Trivia quiz was to be fetched from an API. Open Trivia Database was used and is accessible through the following link: https://opentdb.com/api_config.php

This project was completed with JavaScript, HTML, CSS and Vue.

This finished product was containerized and is accessible via Heroku at this link:
https://frozen-sea-00856.herokuapp.com/

Authors Kristin Åkerlund and Daniel Dumville

